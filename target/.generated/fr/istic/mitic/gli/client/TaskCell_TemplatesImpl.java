package fr.istic.mitic.gli.client;

public class TaskCell_TemplatesImpl implements fr.istic.mitic.gli.client.TaskCell.Templates {
  
  public com.google.gwt.safehtml.shared.SafeHtml makeTaskCell(java.lang.String arg0,java.lang.String arg1,int arg2,java.lang.String arg3) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<div><span class='task-name'>");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
    sb.append("</span></div><div class='task-description'>");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg1));
    sb.append("</div><div class='task-priorite'>");
    sb.append(arg2);
    sb.append("</div><div class='task-etat'>");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg3));
    sb.append("</div>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}

package fr.istic.mitic.gli.client;

public class ProjectCell_TemplatesImpl implements fr.istic.mitic.gli.client.ProjectCell.Templates {
  
  public com.google.gwt.safehtml.shared.SafeHtml makeProjectCell(java.lang.String arg0,int arg1) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<div><span class='project-name'>");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
    sb.append("</span></div><div class='project-id'>");
    sb.append(arg1);
    sb.append("</div>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}

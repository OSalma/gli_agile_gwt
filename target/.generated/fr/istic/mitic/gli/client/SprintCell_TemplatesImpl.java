package fr.istic.mitic.gli.client;

public class SprintCell_TemplatesImpl implements fr.istic.mitic.gli.client.SprintCell.Templates {
  
  public com.google.gwt.safehtml.shared.SafeHtml makeSprintCell(java.lang.String arg0,java.lang.String arg1) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<div><span class='sprint-name'>");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
    sb.append("</span></div><div class='sprint-target'>");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg1));
    sb.append("</div>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}

package fr.istic.mitic.gli.client;

public class DeveloperCell_TemplatesImpl implements fr.istic.mitic.gli.client.DeveloperCell.Templates {
  
  public com.google.gwt.safehtml.shared.SafeHtml makeDeveloperCell(int arg0,java.lang.String arg1) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<div class='developers-list'><div class='developer-id'>Developer ");
    sb.append(arg0);
    sb.append("</div><div class='developer-nom'> ");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg1));
    sb.append("</div></div>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}

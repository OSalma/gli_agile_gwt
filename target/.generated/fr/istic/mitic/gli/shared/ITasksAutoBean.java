package fr.istic.mitic.gli.shared;

public class ITasksAutoBean extends com.google.web.bindery.autobean.shared.impl.AbstractAutoBean<fr.istic.mitic.gli.shared.ITasks> {
  private final fr.istic.mitic.gli.shared.ITasks shim = new fr.istic.mitic.gli.shared.ITasks() {
    public java.util.List getTasks()  {
      java.util.List toReturn = ITasksAutoBean.this.getWrapped().getTasks();
      if (toReturn != null) {
        if (ITasksAutoBean.this.isWrapped(toReturn)) {
          toReturn = ITasksAutoBean.this.getFromWrapper(toReturn);
        } else {
          toReturn = new emul.java.util.ListAutoBean(getFactory(), toReturn).as();
        }
      }
      return toReturn;
    }
    public void setTasks(java.util.List tasks)  {
      ITasksAutoBean.this.getWrapped().setTasks(tasks);
      ITasksAutoBean.this.set("setTasks", tasks);
    }
    @Override public boolean equals(Object o) {
      return this == o || getWrapped().equals(o);
    }
    @Override public int hashCode() {
      return getWrapped().hashCode();
    }
    @Override public String toString() {
      return getWrapped().toString();
    }
  };
  { com.google.gwt.core.client.impl.WeakMapping.set(shim, com.google.web.bindery.autobean.shared.AutoBean.class.getName(), this); }
  public ITasksAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory) {super(factory);}
  public ITasksAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory, fr.istic.mitic.gli.shared.ITasks wrapped) {
    super(wrapped, factory);
  }
  public fr.istic.mitic.gli.shared.ITasks as() {return shim;}
  public Class<fr.istic.mitic.gli.shared.ITasks> getType() {return fr.istic.mitic.gli.shared.ITasks.class;}
  @Override protected fr.istic.mitic.gli.shared.ITasks createSimplePeer() {
    return new fr.istic.mitic.gli.shared.ITasks() {
      private final com.google.web.bindery.autobean.shared.Splittable data = fr.istic.mitic.gli.shared.ITasksAutoBean.this.data;
      public java.util.List getTasks()  {
        return (java.util.List) ITasksAutoBean.this.getOrReify("tasks");
      }
      public void setTasks(java.util.List tasks)  {
        ITasksAutoBean.this.setProperty("tasks", tasks);
      }
    };
  }
  @Override protected void traverseProperties(com.google.web.bindery.autobean.shared.AutoBeanVisitor visitor, com.google.web.bindery.autobean.shared.impl.AbstractAutoBean.OneShotContext ctx) {
    com.google.web.bindery.autobean.shared.impl.AbstractAutoBean bean;
    Object value;
    com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext propertyContext;
    fr.istic.mitic.gli.shared.ITasks as = as();
    bean = (com.google.web.bindery.autobean.shared.impl.AbstractAutoBean) com.google.web.bindery.autobean.shared.AutoBeanUtils.getAutoBean(as.getTasks());
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(ITasksAutoBean.this, "tasks"),
      new Class<?>[] {java.util.List.class, fr.istic.mitic.gli.shared.ITask.class},
      new int[] {1, 0}
    );
    if (visitor.visitCollectionProperty("tasks", bean, propertyContext)) {
      if (bean != null) { bean.traverse(visitor, ctx); }
    }
    visitor.endVisitCollectionProperty("tasks", bean, propertyContext);
  }
}

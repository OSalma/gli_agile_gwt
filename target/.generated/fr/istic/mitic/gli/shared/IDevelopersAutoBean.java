package fr.istic.mitic.gli.shared;

public class IDevelopersAutoBean extends com.google.web.bindery.autobean.shared.impl.AbstractAutoBean<fr.istic.mitic.gli.shared.IDevelopers> {
  private final fr.istic.mitic.gli.shared.IDevelopers shim = new fr.istic.mitic.gli.shared.IDevelopers() {
    public java.util.List getDevelopers()  {
      java.util.List toReturn = IDevelopersAutoBean.this.getWrapped().getDevelopers();
      if (toReturn != null) {
        if (IDevelopersAutoBean.this.isWrapped(toReturn)) {
          toReturn = IDevelopersAutoBean.this.getFromWrapper(toReturn);
        } else {
          toReturn = new emul.java.util.ListAutoBean(getFactory(), toReturn).as();
        }
      }
      return toReturn;
    }
    public void setDevelopers(java.util.List developers)  {
      IDevelopersAutoBean.this.getWrapped().setDevelopers(developers);
      IDevelopersAutoBean.this.set("setDevelopers", developers);
    }
    @Override public boolean equals(Object o) {
      return this == o || getWrapped().equals(o);
    }
    @Override public int hashCode() {
      return getWrapped().hashCode();
    }
    @Override public String toString() {
      return getWrapped().toString();
    }
  };
  { com.google.gwt.core.client.impl.WeakMapping.set(shim, com.google.web.bindery.autobean.shared.AutoBean.class.getName(), this); }
  public IDevelopersAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory) {super(factory);}
  public IDevelopersAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory, fr.istic.mitic.gli.shared.IDevelopers wrapped) {
    super(wrapped, factory);
  }
  public fr.istic.mitic.gli.shared.IDevelopers as() {return shim;}
  public Class<fr.istic.mitic.gli.shared.IDevelopers> getType() {return fr.istic.mitic.gli.shared.IDevelopers.class;}
  @Override protected fr.istic.mitic.gli.shared.IDevelopers createSimplePeer() {
    return new fr.istic.mitic.gli.shared.IDevelopers() {
      private final com.google.web.bindery.autobean.shared.Splittable data = fr.istic.mitic.gli.shared.IDevelopersAutoBean.this.data;
      public java.util.List getDevelopers()  {
        return (java.util.List) IDevelopersAutoBean.this.getOrReify("developers");
      }
      public void setDevelopers(java.util.List developers)  {
        IDevelopersAutoBean.this.setProperty("developers", developers);
      }
    };
  }
  @Override protected void traverseProperties(com.google.web.bindery.autobean.shared.AutoBeanVisitor visitor, com.google.web.bindery.autobean.shared.impl.AbstractAutoBean.OneShotContext ctx) {
    com.google.web.bindery.autobean.shared.impl.AbstractAutoBean bean;
    Object value;
    com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext propertyContext;
    fr.istic.mitic.gli.shared.IDevelopers as = as();
    bean = (com.google.web.bindery.autobean.shared.impl.AbstractAutoBean) com.google.web.bindery.autobean.shared.AutoBeanUtils.getAutoBean(as.getDevelopers());
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(IDevelopersAutoBean.this, "developers"),
      new Class<?>[] {java.util.List.class, fr.istic.mitic.gli.shared.IDeveloper.class},
      new int[] {1, 0}
    );
    if (visitor.visitCollectionProperty("developers", bean, propertyContext)) {
      if (bean != null) { bean.traverse(visitor, ctx); }
    }
    visitor.endVisitCollectionProperty("developers", bean, propertyContext);
  }
}

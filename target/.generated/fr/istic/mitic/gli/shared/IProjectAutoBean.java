package fr.istic.mitic.gli.shared;

public class IProjectAutoBean extends com.google.web.bindery.autobean.shared.impl.AbstractAutoBean<fr.istic.mitic.gli.shared.IProject> {
  private final fr.istic.mitic.gli.shared.IProject shim = new fr.istic.mitic.gli.shared.IProject() {
    public int getId()  {
      int toReturn = IProjectAutoBean.this.getWrapped().getId();
      return toReturn;
    }
    public java.lang.String getNom()  {
      java.lang.String toReturn = IProjectAutoBean.this.getWrapped().getNom();
      return toReturn;
    }
    public void setId(int id)  {
      IProjectAutoBean.this.getWrapped().setId(id);
      IProjectAutoBean.this.set("setId", id);
    }
    public void setNom(java.lang.String nom)  {
      IProjectAutoBean.this.getWrapped().setNom(nom);
      IProjectAutoBean.this.set("setNom", nom);
    }
    @Override public boolean equals(Object o) {
      return this == o || getWrapped().equals(o);
    }
    @Override public int hashCode() {
      return getWrapped().hashCode();
    }
    @Override public String toString() {
      return getWrapped().toString();
    }
  };
  { com.google.gwt.core.client.impl.WeakMapping.set(shim, com.google.web.bindery.autobean.shared.AutoBean.class.getName(), this); }
  public IProjectAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory) {super(factory);}
  public IProjectAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory, fr.istic.mitic.gli.shared.IProject wrapped) {
    super(wrapped, factory);
  }
  public fr.istic.mitic.gli.shared.IProject as() {return shim;}
  public Class<fr.istic.mitic.gli.shared.IProject> getType() {return fr.istic.mitic.gli.shared.IProject.class;}
  @Override protected fr.istic.mitic.gli.shared.IProject createSimplePeer() {
    return new fr.istic.mitic.gli.shared.IProject() {
      private final com.google.web.bindery.autobean.shared.Splittable data = fr.istic.mitic.gli.shared.IProjectAutoBean.this.data;
      public int getId()  {
        java.lang.Integer toReturn = IProjectAutoBean.this.getOrReify("id");
        return toReturn == null ? 0 : toReturn;
      }
      public java.lang.String getNom()  {
        return (java.lang.String) IProjectAutoBean.this.getOrReify("nom");
      }
      public void setId(int id)  {
        IProjectAutoBean.this.setProperty("id", id);
      }
      public void setNom(java.lang.String nom)  {
        IProjectAutoBean.this.setProperty("nom", nom);
      }
    };
  }
  @Override protected void traverseProperties(com.google.web.bindery.autobean.shared.AutoBeanVisitor visitor, com.google.web.bindery.autobean.shared.impl.AbstractAutoBean.OneShotContext ctx) {
    com.google.web.bindery.autobean.shared.impl.AbstractAutoBean bean;
    Object value;
    com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext propertyContext;
    fr.istic.mitic.gli.shared.IProject as = as();
    value = as.getId();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(IProjectAutoBean.this, "id"),
      int.class
    );
    if (visitor.visitValueProperty("id", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("id", value, propertyContext);
    value = as.getNom();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(IProjectAutoBean.this, "nom"),
      java.lang.String.class
    );
    if (visitor.visitValueProperty("nom", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("nom", value, propertyContext);
  }
}

package fr.istic.mitic.gli.shared;

public class ITaskAutoBean extends com.google.web.bindery.autobean.shared.impl.AbstractAutoBean<fr.istic.mitic.gli.shared.ITask> {
  private final fr.istic.mitic.gli.shared.ITask shim = new fr.istic.mitic.gli.shared.ITask() {
    public int getPriorite()  {
      int toReturn = ITaskAutoBean.this.getWrapped().getPriorite();
      return toReturn;
    }
    public java.lang.Long getId()  {
      java.lang.Long toReturn = ITaskAutoBean.this.getWrapped().getId();
      return toReturn;
    }
    public java.lang.String getDescription()  {
      java.lang.String toReturn = ITaskAutoBean.this.getWrapped().getDescription();
      return toReturn;
    }
    public java.lang.String getEtat()  {
      java.lang.String toReturn = ITaskAutoBean.this.getWrapped().getEtat();
      return toReturn;
    }
    public java.lang.String getNom()  {
      java.lang.String toReturn = ITaskAutoBean.this.getWrapped().getNom();
      return toReturn;
    }
    public void setDescription(java.lang.String description)  {
      ITaskAutoBean.this.getWrapped().setDescription(description);
      ITaskAutoBean.this.set("setDescription", description);
    }
    public void setEtat(java.lang.String etat)  {
      ITaskAutoBean.this.getWrapped().setEtat(etat);
      ITaskAutoBean.this.set("setEtat", etat);
    }
    public void setId(java.lang.Long id)  {
      ITaskAutoBean.this.getWrapped().setId(id);
      ITaskAutoBean.this.set("setId", id);
    }
    public void setNom(java.lang.String nom)  {
      ITaskAutoBean.this.getWrapped().setNom(nom);
      ITaskAutoBean.this.set("setNom", nom);
    }
    public void setPriorite(int priorite)  {
      ITaskAutoBean.this.getWrapped().setPriorite(priorite);
      ITaskAutoBean.this.set("setPriorite", priorite);
    }
    @Override public boolean equals(Object o) {
      return this == o || getWrapped().equals(o);
    }
    @Override public int hashCode() {
      return getWrapped().hashCode();
    }
    @Override public String toString() {
      return getWrapped().toString();
    }
  };
  { com.google.gwt.core.client.impl.WeakMapping.set(shim, com.google.web.bindery.autobean.shared.AutoBean.class.getName(), this); }
  public ITaskAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory) {super(factory);}
  public ITaskAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory, fr.istic.mitic.gli.shared.ITask wrapped) {
    super(wrapped, factory);
  }
  public fr.istic.mitic.gli.shared.ITask as() {return shim;}
  public Class<fr.istic.mitic.gli.shared.ITask> getType() {return fr.istic.mitic.gli.shared.ITask.class;}
  @Override protected fr.istic.mitic.gli.shared.ITask createSimplePeer() {
    return new fr.istic.mitic.gli.shared.ITask() {
      private final com.google.web.bindery.autobean.shared.Splittable data = fr.istic.mitic.gli.shared.ITaskAutoBean.this.data;
      public int getPriorite()  {
        java.lang.Integer toReturn = ITaskAutoBean.this.getOrReify("priorite");
        return toReturn == null ? 0 : toReturn;
      }
      public java.lang.Long getId()  {
        return (java.lang.Long) ITaskAutoBean.this.getOrReify("id");
      }
      public java.lang.String getDescription()  {
        return (java.lang.String) ITaskAutoBean.this.getOrReify("description");
      }
      public java.lang.String getEtat()  {
        return (java.lang.String) ITaskAutoBean.this.getOrReify("etat");
      }
      public java.lang.String getNom()  {
        return (java.lang.String) ITaskAutoBean.this.getOrReify("nom");
      }
      public void setDescription(java.lang.String description)  {
        ITaskAutoBean.this.setProperty("description", description);
      }
      public void setEtat(java.lang.String etat)  {
        ITaskAutoBean.this.setProperty("etat", etat);
      }
      public void setId(java.lang.Long id)  {
        ITaskAutoBean.this.setProperty("id", id);
      }
      public void setNom(java.lang.String nom)  {
        ITaskAutoBean.this.setProperty("nom", nom);
      }
      public void setPriorite(int priorite)  {
        ITaskAutoBean.this.setProperty("priorite", priorite);
      }
    };
  }
  @Override protected void traverseProperties(com.google.web.bindery.autobean.shared.AutoBeanVisitor visitor, com.google.web.bindery.autobean.shared.impl.AbstractAutoBean.OneShotContext ctx) {
    com.google.web.bindery.autobean.shared.impl.AbstractAutoBean bean;
    Object value;
    com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext propertyContext;
    fr.istic.mitic.gli.shared.ITask as = as();
    value = as.getPriorite();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(ITaskAutoBean.this, "priorite"),
      int.class
    );
    if (visitor.visitValueProperty("priorite", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("priorite", value, propertyContext);
    value = as.getId();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(ITaskAutoBean.this, "id"),
      java.lang.Long.class
    );
    if (visitor.visitValueProperty("id", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("id", value, propertyContext);
    value = as.getDescription();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(ITaskAutoBean.this, "description"),
      java.lang.String.class
    );
    if (visitor.visitValueProperty("description", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("description", value, propertyContext);
    value = as.getEtat();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(ITaskAutoBean.this, "etat"),
      java.lang.String.class
    );
    if (visitor.visitValueProperty("etat", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("etat", value, propertyContext);
    value = as.getNom();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(ITaskAutoBean.this, "nom"),
      java.lang.String.class
    );
    if (visitor.visitValueProperty("nom", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("nom", value, propertyContext);
  }
}

package fr.istic.mitic.gli.shared;

public class IProjectsAutoBean extends com.google.web.bindery.autobean.shared.impl.AbstractAutoBean<fr.istic.mitic.gli.shared.IProjects> {
  private final fr.istic.mitic.gli.shared.IProjects shim = new fr.istic.mitic.gli.shared.IProjects() {
    public java.util.List getProjects()  {
      java.util.List toReturn = IProjectsAutoBean.this.getWrapped().getProjects();
      if (toReturn != null) {
        if (IProjectsAutoBean.this.isWrapped(toReturn)) {
          toReturn = IProjectsAutoBean.this.getFromWrapper(toReturn);
        } else {
          toReturn = new emul.java.util.ListAutoBean(getFactory(), toReturn).as();
        }
      }
      return toReturn;
    }
    public void setProjects(java.util.List projects)  {
      IProjectsAutoBean.this.getWrapped().setProjects(projects);
      IProjectsAutoBean.this.set("setProjects", projects);
    }
    @Override public boolean equals(Object o) {
      return this == o || getWrapped().equals(o);
    }
    @Override public int hashCode() {
      return getWrapped().hashCode();
    }
    @Override public String toString() {
      return getWrapped().toString();
    }
  };
  { com.google.gwt.core.client.impl.WeakMapping.set(shim, com.google.web.bindery.autobean.shared.AutoBean.class.getName(), this); }
  public IProjectsAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory) {super(factory);}
  public IProjectsAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory, fr.istic.mitic.gli.shared.IProjects wrapped) {
    super(wrapped, factory);
  }
  public fr.istic.mitic.gli.shared.IProjects as() {return shim;}
  public Class<fr.istic.mitic.gli.shared.IProjects> getType() {return fr.istic.mitic.gli.shared.IProjects.class;}
  @Override protected fr.istic.mitic.gli.shared.IProjects createSimplePeer() {
    return new fr.istic.mitic.gli.shared.IProjects() {
      private final com.google.web.bindery.autobean.shared.Splittable data = fr.istic.mitic.gli.shared.IProjectsAutoBean.this.data;
      public java.util.List getProjects()  {
        return (java.util.List) IProjectsAutoBean.this.getOrReify("projects");
      }
      public void setProjects(java.util.List projects)  {
        IProjectsAutoBean.this.setProperty("projects", projects);
      }
    };
  }
  @Override protected void traverseProperties(com.google.web.bindery.autobean.shared.AutoBeanVisitor visitor, com.google.web.bindery.autobean.shared.impl.AbstractAutoBean.OneShotContext ctx) {
    com.google.web.bindery.autobean.shared.impl.AbstractAutoBean bean;
    Object value;
    com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext propertyContext;
    fr.istic.mitic.gli.shared.IProjects as = as();
    bean = (com.google.web.bindery.autobean.shared.impl.AbstractAutoBean) com.google.web.bindery.autobean.shared.AutoBeanUtils.getAutoBean(as.getProjects());
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(IProjectsAutoBean.this, "projects"),
      new Class<?>[] {java.util.List.class, fr.istic.mitic.gli.shared.IProject.class},
      new int[] {1, 0}
    );
    if (visitor.visitCollectionProperty("projects", bean, propertyContext)) {
      if (bean != null) { bean.traverse(visitor, ctx); }
    }
    visitor.endVisitCollectionProperty("projects", bean, propertyContext);
  }
}

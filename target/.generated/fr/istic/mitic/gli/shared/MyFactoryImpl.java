package fr.istic.mitic.gli.shared;

public class MyFactoryImpl extends com.google.web.bindery.autobean.gwt.client.impl.AbstractAutoBeanFactory implements fr.istic.mitic.gli.shared.MyFactory {
  @Override protected void initializeCreatorMap(com.google.web.bindery.autobean.gwt.client.impl.JsniCreatorMap map) {
    map.add(fr.istic.mitic.gli.shared.IDevelopers.class, getConstructors_fr_istic_mitic_gli_shared_IDevelopers());
    map.add(fr.istic.mitic.gli.shared.IDeveloper.class, getConstructors_fr_istic_mitic_gli_shared_IDeveloper());
    map.add(fr.istic.mitic.gli.shared.IProject.class, getConstructors_fr_istic_mitic_gli_shared_IProject());
    map.add(fr.istic.mitic.gli.shared.IProjects.class, getConstructors_fr_istic_mitic_gli_shared_IProjects());
    map.add(fr.istic.mitic.gli.shared.ISprint.class, getConstructors_fr_istic_mitic_gli_shared_ISprint());
    map.add(fr.istic.mitic.gli.shared.ISprints.class, getConstructors_fr_istic_mitic_gli_shared_ISprints());
    map.add(fr.istic.mitic.gli.shared.ITask.class, getConstructors_fr_istic_mitic_gli_shared_ITask());
    map.add(fr.istic.mitic.gli.shared.ITasks.class, getConstructors_fr_istic_mitic_gli_shared_ITasks());
    map.add(java.util.List.class, getConstructors_java_util_List());
    map.add(java.util.Iterator.class, getConstructors_java_util_Iterator());
    map.add(java.util.ListIterator.class, getConstructors_java_util_ListIterator());
  }
  private native com.google.gwt.core.client.JsArray<com.google.gwt.core.client.JavaScriptObject> getConstructors_fr_istic_mitic_gli_shared_IDevelopers() /*-{
    return [
      @fr.istic.mitic.gli.shared.IDevelopersAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;),
      @fr.istic.mitic.gli.shared.IDevelopersAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;Lfr/istic/mitic/gli/shared/IDevelopers;)
    ];
  }-*/;
  private native com.google.gwt.core.client.JsArray<com.google.gwt.core.client.JavaScriptObject> getConstructors_fr_istic_mitic_gli_shared_IDeveloper() /*-{
    return [
      @fr.istic.mitic.gli.shared.IDeveloperAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;),
      @fr.istic.mitic.gli.shared.IDeveloperAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;Lfr/istic/mitic/gli/shared/IDeveloper;)
    ];
  }-*/;
  private native com.google.gwt.core.client.JsArray<com.google.gwt.core.client.JavaScriptObject> getConstructors_fr_istic_mitic_gli_shared_IProject() /*-{
    return [
      @fr.istic.mitic.gli.shared.IProjectAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;),
      @fr.istic.mitic.gli.shared.IProjectAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;Lfr/istic/mitic/gli/shared/IProject;)
    ];
  }-*/;
  private native com.google.gwt.core.client.JsArray<com.google.gwt.core.client.JavaScriptObject> getConstructors_fr_istic_mitic_gli_shared_IProjects() /*-{
    return [
      @fr.istic.mitic.gli.shared.IProjectsAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;),
      @fr.istic.mitic.gli.shared.IProjectsAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;Lfr/istic/mitic/gli/shared/IProjects;)
    ];
  }-*/;
  private native com.google.gwt.core.client.JsArray<com.google.gwt.core.client.JavaScriptObject> getConstructors_fr_istic_mitic_gli_shared_ISprint() /*-{
    return [
      @fr.istic.mitic.gli.shared.ISprintAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;),
      @fr.istic.mitic.gli.shared.ISprintAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;Lfr/istic/mitic/gli/shared/ISprint;)
    ];
  }-*/;
  private native com.google.gwt.core.client.JsArray<com.google.gwt.core.client.JavaScriptObject> getConstructors_fr_istic_mitic_gli_shared_ISprints() /*-{
    return [
      @fr.istic.mitic.gli.shared.ISprintsAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;),
      @fr.istic.mitic.gli.shared.ISprintsAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;Lfr/istic/mitic/gli/shared/ISprints;)
    ];
  }-*/;
  private native com.google.gwt.core.client.JsArray<com.google.gwt.core.client.JavaScriptObject> getConstructors_fr_istic_mitic_gli_shared_ITask() /*-{
    return [
      @fr.istic.mitic.gli.shared.ITaskAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;),
      @fr.istic.mitic.gli.shared.ITaskAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;Lfr/istic/mitic/gli/shared/ITask;)
    ];
  }-*/;
  private native com.google.gwt.core.client.JsArray<com.google.gwt.core.client.JavaScriptObject> getConstructors_fr_istic_mitic_gli_shared_ITasks() /*-{
    return [
      @fr.istic.mitic.gli.shared.ITasksAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;),
      @fr.istic.mitic.gli.shared.ITasksAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;Lfr/istic/mitic/gli/shared/ITasks;)
    ];
  }-*/;
  private native com.google.gwt.core.client.JsArray<com.google.gwt.core.client.JavaScriptObject> getConstructors_java_util_List() /*-{
    return [
      ,
      @emul.java.util.ListAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;Ljava/util/List;)
    ];
  }-*/;
  private native com.google.gwt.core.client.JsArray<com.google.gwt.core.client.JavaScriptObject> getConstructors_java_util_Iterator() /*-{
    return [
      ,
      @emul.java.util.IteratorAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;Ljava/util/Iterator;)
    ];
  }-*/;
  private native com.google.gwt.core.client.JsArray<com.google.gwt.core.client.JavaScriptObject> getConstructors_java_util_ListIterator() /*-{
    return [
      ,
      @emul.java.util.ListIteratorAutoBean::new(Lcom/google/web/bindery/autobean/shared/AutoBeanFactory;Ljava/util/ListIterator;)
    ];
  }-*/;
  @Override protected void initializeEnumMap() {
  }
  public com.google.web.bindery.autobean.shared.AutoBean develoeprs() {
    return new fr.istic.mitic.gli.shared.IDevelopersAutoBean(MyFactoryImpl.this);
  }
  public com.google.web.bindery.autobean.shared.AutoBean developer() {
    return new fr.istic.mitic.gli.shared.IDeveloperAutoBean(MyFactoryImpl.this);
  }
  public com.google.web.bindery.autobean.shared.AutoBean project() {
    return new fr.istic.mitic.gli.shared.IProjectAutoBean(MyFactoryImpl.this);
  }
  public com.google.web.bindery.autobean.shared.AutoBean projects() {
    return new fr.istic.mitic.gli.shared.IProjectsAutoBean(MyFactoryImpl.this);
  }
  public com.google.web.bindery.autobean.shared.AutoBean sprint() {
    return new fr.istic.mitic.gli.shared.ISprintAutoBean(MyFactoryImpl.this);
  }
  public com.google.web.bindery.autobean.shared.AutoBean sprints() {
    return new fr.istic.mitic.gli.shared.ISprintsAutoBean(MyFactoryImpl.this);
  }
  public com.google.web.bindery.autobean.shared.AutoBean task() {
    return new fr.istic.mitic.gli.shared.ITaskAutoBean(MyFactoryImpl.this);
  }
  public com.google.web.bindery.autobean.shared.AutoBean tasks() {
    return new fr.istic.mitic.gli.shared.ITasksAutoBean(MyFactoryImpl.this);
  }
}

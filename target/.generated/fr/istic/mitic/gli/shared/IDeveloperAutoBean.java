package fr.istic.mitic.gli.shared;

public class IDeveloperAutoBean extends com.google.web.bindery.autobean.shared.impl.AbstractAutoBean<fr.istic.mitic.gli.shared.IDeveloper> {
  private final fr.istic.mitic.gli.shared.IDeveloper shim = new fr.istic.mitic.gli.shared.IDeveloper() {
    public int getId()  {
      int toReturn = IDeveloperAutoBean.this.getWrapped().getId();
      return toReturn;
    }
    public java.lang.String getNom()  {
      java.lang.String toReturn = IDeveloperAutoBean.this.getWrapped().getNom();
      return toReturn;
    }
    public void setId(int id)  {
      IDeveloperAutoBean.this.getWrapped().setId(id);
      IDeveloperAutoBean.this.set("setId", id);
    }
    public void setNom(java.lang.String nom)  {
      IDeveloperAutoBean.this.getWrapped().setNom(nom);
      IDeveloperAutoBean.this.set("setNom", nom);
    }
    @Override public boolean equals(Object o) {
      return this == o || getWrapped().equals(o);
    }
    @Override public int hashCode() {
      return getWrapped().hashCode();
    }
    @Override public String toString() {
      return getWrapped().toString();
    }
  };
  { com.google.gwt.core.client.impl.WeakMapping.set(shim, com.google.web.bindery.autobean.shared.AutoBean.class.getName(), this); }
  public IDeveloperAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory) {super(factory);}
  public IDeveloperAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory, fr.istic.mitic.gli.shared.IDeveloper wrapped) {
    super(wrapped, factory);
  }
  public fr.istic.mitic.gli.shared.IDeveloper as() {return shim;}
  public Class<fr.istic.mitic.gli.shared.IDeveloper> getType() {return fr.istic.mitic.gli.shared.IDeveloper.class;}
  @Override protected fr.istic.mitic.gli.shared.IDeveloper createSimplePeer() {
    return new fr.istic.mitic.gli.shared.IDeveloper() {
      private final com.google.web.bindery.autobean.shared.Splittable data = fr.istic.mitic.gli.shared.IDeveloperAutoBean.this.data;
      public int getId()  {
        java.lang.Integer toReturn = IDeveloperAutoBean.this.getOrReify("id");
        return toReturn == null ? 0 : toReturn;
      }
      public java.lang.String getNom()  {
        return (java.lang.String) IDeveloperAutoBean.this.getOrReify("nom");
      }
      public void setId(int id)  {
        IDeveloperAutoBean.this.setProperty("id", id);
      }
      public void setNom(java.lang.String nom)  {
        IDeveloperAutoBean.this.setProperty("nom", nom);
      }
    };
  }
  @Override protected void traverseProperties(com.google.web.bindery.autobean.shared.AutoBeanVisitor visitor, com.google.web.bindery.autobean.shared.impl.AbstractAutoBean.OneShotContext ctx) {
    com.google.web.bindery.autobean.shared.impl.AbstractAutoBean bean;
    Object value;
    com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext propertyContext;
    fr.istic.mitic.gli.shared.IDeveloper as = as();
    value = as.getId();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(IDeveloperAutoBean.this, "id"),
      int.class
    );
    if (visitor.visitValueProperty("id", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("id", value, propertyContext);
    value = as.getNom();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(IDeveloperAutoBean.this, "nom"),
      java.lang.String.class
    );
    if (visitor.visitValueProperty("nom", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("nom", value, propertyContext);
  }
}

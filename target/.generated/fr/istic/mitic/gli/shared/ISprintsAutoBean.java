package fr.istic.mitic.gli.shared;

public class ISprintsAutoBean extends com.google.web.bindery.autobean.shared.impl.AbstractAutoBean<fr.istic.mitic.gli.shared.ISprints> {
  private final fr.istic.mitic.gli.shared.ISprints shim = new fr.istic.mitic.gli.shared.ISprints() {
    public java.util.List getSprints()  {
      java.util.List toReturn = ISprintsAutoBean.this.getWrapped().getSprints();
      if (toReturn != null) {
        if (ISprintsAutoBean.this.isWrapped(toReturn)) {
          toReturn = ISprintsAutoBean.this.getFromWrapper(toReturn);
        } else {
          toReturn = new emul.java.util.ListAutoBean(getFactory(), toReturn).as();
        }
      }
      return toReturn;
    }
    public void setSprints(java.util.List sprints)  {
      ISprintsAutoBean.this.getWrapped().setSprints(sprints);
      ISprintsAutoBean.this.set("setSprints", sprints);
    }
    @Override public boolean equals(Object o) {
      return this == o || getWrapped().equals(o);
    }
    @Override public int hashCode() {
      return getWrapped().hashCode();
    }
    @Override public String toString() {
      return getWrapped().toString();
    }
  };
  { com.google.gwt.core.client.impl.WeakMapping.set(shim, com.google.web.bindery.autobean.shared.AutoBean.class.getName(), this); }
  public ISprintsAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory) {super(factory);}
  public ISprintsAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory, fr.istic.mitic.gli.shared.ISprints wrapped) {
    super(wrapped, factory);
  }
  public fr.istic.mitic.gli.shared.ISprints as() {return shim;}
  public Class<fr.istic.mitic.gli.shared.ISprints> getType() {return fr.istic.mitic.gli.shared.ISprints.class;}
  @Override protected fr.istic.mitic.gli.shared.ISprints createSimplePeer() {
    return new fr.istic.mitic.gli.shared.ISprints() {
      private final com.google.web.bindery.autobean.shared.Splittable data = fr.istic.mitic.gli.shared.ISprintsAutoBean.this.data;
      public java.util.List getSprints()  {
        return (java.util.List) ISprintsAutoBean.this.getOrReify("sprints");
      }
      public void setSprints(java.util.List sprints)  {
        ISprintsAutoBean.this.setProperty("sprints", sprints);
      }
    };
  }
  @Override protected void traverseProperties(com.google.web.bindery.autobean.shared.AutoBeanVisitor visitor, com.google.web.bindery.autobean.shared.impl.AbstractAutoBean.OneShotContext ctx) {
    com.google.web.bindery.autobean.shared.impl.AbstractAutoBean bean;
    Object value;
    com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext propertyContext;
    fr.istic.mitic.gli.shared.ISprints as = as();
    bean = (com.google.web.bindery.autobean.shared.impl.AbstractAutoBean) com.google.web.bindery.autobean.shared.AutoBeanUtils.getAutoBean(as.getSprints());
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(ISprintsAutoBean.this, "sprints"),
      new Class<?>[] {java.util.List.class, fr.istic.mitic.gli.shared.ISprint.class},
      new int[] {1, 0}
    );
    if (visitor.visitCollectionProperty("sprints", bean, propertyContext)) {
      if (bean != null) { bean.traverse(visitor, ctx); }
    }
    visitor.endVisitCollectionProperty("sprints", bean, propertyContext);
  }
}

package fr.istic.mitic.gli.shared;

public class ISprintAutoBean extends com.google.web.bindery.autobean.shared.impl.AbstractAutoBean<fr.istic.mitic.gli.shared.ISprint> {
  private final fr.istic.mitic.gli.shared.ISprint shim = new fr.istic.mitic.gli.shared.ISprint() {
    public int getId()  {
      int toReturn = ISprintAutoBean.this.getWrapped().getId();
      return toReturn;
    }
    public int getIdProject()  {
      int toReturn = ISprintAutoBean.this.getWrapped().getIdProject();
      return toReturn;
    }
    public java.lang.String getNom()  {
      java.lang.String toReturn = ISprintAutoBean.this.getWrapped().getNom();
      return toReturn;
    }
    public java.lang.String getTarget()  {
      java.lang.String toReturn = ISprintAutoBean.this.getWrapped().getTarget();
      return toReturn;
    }
    public void setId(int id)  {
      ISprintAutoBean.this.getWrapped().setId(id);
      ISprintAutoBean.this.set("setId", id);
    }
    public void setIdProject(int id)  {
      ISprintAutoBean.this.getWrapped().setIdProject(id);
      ISprintAutoBean.this.set("setIdProject", id);
    }
    public void setNom(java.lang.String nom)  {
      ISprintAutoBean.this.getWrapped().setNom(nom);
      ISprintAutoBean.this.set("setNom", nom);
    }
    public void setTarget(java.lang.String target)  {
      ISprintAutoBean.this.getWrapped().setTarget(target);
      ISprintAutoBean.this.set("setTarget", target);
    }
    @Override public boolean equals(Object o) {
      return this == o || getWrapped().equals(o);
    }
    @Override public int hashCode() {
      return getWrapped().hashCode();
    }
    @Override public String toString() {
      return getWrapped().toString();
    }
  };
  { com.google.gwt.core.client.impl.WeakMapping.set(shim, com.google.web.bindery.autobean.shared.AutoBean.class.getName(), this); }
  public ISprintAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory) {super(factory);}
  public ISprintAutoBean(com.google.web.bindery.autobean.shared.AutoBeanFactory factory, fr.istic.mitic.gli.shared.ISprint wrapped) {
    super(wrapped, factory);
  }
  public fr.istic.mitic.gli.shared.ISprint as() {return shim;}
  public Class<fr.istic.mitic.gli.shared.ISprint> getType() {return fr.istic.mitic.gli.shared.ISprint.class;}
  @Override protected fr.istic.mitic.gli.shared.ISprint createSimplePeer() {
    return new fr.istic.mitic.gli.shared.ISprint() {
      private final com.google.web.bindery.autobean.shared.Splittable data = fr.istic.mitic.gli.shared.ISprintAutoBean.this.data;
      public int getId()  {
        java.lang.Integer toReturn = ISprintAutoBean.this.getOrReify("id");
        return toReturn == null ? 0 : toReturn;
      }
      public int getIdProject()  {
        java.lang.Integer toReturn = ISprintAutoBean.this.getOrReify("idProject");
        return toReturn == null ? 0 : toReturn;
      }
      public java.lang.String getNom()  {
        return (java.lang.String) ISprintAutoBean.this.getOrReify("nom");
      }
      public java.lang.String getTarget()  {
        return (java.lang.String) ISprintAutoBean.this.getOrReify("target");
      }
      public void setId(int id)  {
        ISprintAutoBean.this.setProperty("id", id);
      }
      public void setIdProject(int id)  {
        ISprintAutoBean.this.setProperty("idProject", id);
      }
      public void setNom(java.lang.String nom)  {
        ISprintAutoBean.this.setProperty("nom", nom);
      }
      public void setTarget(java.lang.String target)  {
        ISprintAutoBean.this.setProperty("target", target);
      }
    };
  }
  @Override protected void traverseProperties(com.google.web.bindery.autobean.shared.AutoBeanVisitor visitor, com.google.web.bindery.autobean.shared.impl.AbstractAutoBean.OneShotContext ctx) {
    com.google.web.bindery.autobean.shared.impl.AbstractAutoBean bean;
    Object value;
    com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext propertyContext;
    fr.istic.mitic.gli.shared.ISprint as = as();
    value = as.getId();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(ISprintAutoBean.this, "id"),
      int.class
    );
    if (visitor.visitValueProperty("id", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("id", value, propertyContext);
    value = as.getIdProject();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(ISprintAutoBean.this, "idProject"),
      int.class
    );
    if (visitor.visitValueProperty("idProject", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("idProject", value, propertyContext);
    value = as.getNom();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(ISprintAutoBean.this, "nom"),
      java.lang.String.class
    );
    if (visitor.visitValueProperty("nom", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("nom", value, propertyContext);
    value = as.getTarget();
    propertyContext = new com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext(
      as,
      com.google.web.bindery.autobean.gwt.client.impl.ClientPropertyContext.Setter.beanSetter(ISprintAutoBean.this, "target"),
      java.lang.String.class
    );
    if (visitor.visitValueProperty("target", value, propertyContext)) {
    }
    visitor.endVisitValueProperty("target", value, propertyContext);
  }
}

package fr.istic.mitic.gli.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import fr.istic.mitic.gli.jsonConverter.DeveloperJsonConverter;
import fr.istic.mitic.gli.jsonConverter.DevelopersJsonConverter;
import fr.istic.mitic.gli.jsonConverter.ProjectJsonConverter;
import fr.istic.mitic.gli.jsonConverter.ProjectsJsonConverter;
import fr.istic.mitic.gli.jsonConverter.SprintJsonConverter;
import fr.istic.mitic.gli.jsonConverter.SprintsJsonConverter;
import fr.istic.mitic.gli.jsonConverter.TaskJsonConverter;
import fr.istic.mitic.gli.jsonConverter.TasksJsonConverter;
import fr.istic.mitic.gli.shared.IDeveloper;
import fr.istic.mitic.gli.shared.IDevelopers;
import fr.istic.mitic.gli.shared.IProject;
import fr.istic.mitic.gli.shared.IProjects;
import fr.istic.mitic.gli.shared.ISprint;
import fr.istic.mitic.gli.shared.ISprints;
import fr.istic.mitic.gli.shared.ITask;
import fr.istic.mitic.gli.shared.ITasks;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class agileDashbord implements EntryPoint {
	
	private static final String REST_API_URL = "http://localhost:8080";
	
	private CellList<IProject> projectList;
	private CellList<ISprint> sprintList;
	private CellList<ITask> taskList;
	private CellList<IDeveloper> developerList;
		
	private IProject selectedProject;
	private ISprint selectedSprint;
	private ITask selectedTask;
	private IDeveloper selectedDeveloper;
	
	private Button deleteProjectButton, deleteSprintButton, 
					deleteTaskButton, deleteDeveloperButton;
	
	private VerticalPanel verticalPanelProject, verticalPanelSprint, 
							verticalPanelTask, verticalPanelDeveloper;
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		
		RootPanel rootPanel = RootPanel.get();
		HorizontalPanel globalPanel = new HorizontalPanel();
				
		/* ------- Panel project ------- */
		verticalPanelProject = new VerticalPanel();
		Label labelProject = new Label("Project");
		labelProject.setStyleName("labels");
		Button addProjectButton = new Button("Add Project");
		addProjectButton.setStyleName("buttons");
		addProjectButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				AddProjectDialog dialog = new AddProjectDialog(agileDashbord.this);
				dialog.center();
			}
		});
		
		deleteProjectButton = new Button("Delete Project");
		deleteProjectButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				if (selectedProject != null) {
					deleteProject(selectedProject);
					loadAllProjects();
				}
			}
		});	
		deleteProjectButton.setEnabled(false);
		HorizontalPanel buttonHori = new HorizontalPanel();
		buttonHori.add(addProjectButton);
		buttonHori.add(deleteProjectButton);
		buttonHori.setStyleName("buttons");
		verticalPanelProject.add(labelProject);
		verticalPanelProject.add(buttonHori);
		//verticalPanel.setStyleName("verticalPanels");
		verticalPanelProject.setSize("250px", "100%");
		verticalPanelProject.getElement().setAttribute("cellspacing", "10");
		addProjectButton.setStyleName("buttonsAdd");
		deleteProjectButton.setStyleName("buttonsDel");
		
		ProvidesKey<IProject> projectKeyProvider = new ProvidesKey<IProject>() {
			public Object getKey(IProject item) {
				return (item == null) ? null : item.getId();
			}
		};
		// Create a CellList using the keyProvider, for the projects list
		projectList = new CellList<IProject>(new ProjectCell(), projectKeyProvider);
		final SingleSelectionModel<IProject> selectionModel = new SingleSelectionModel<IProject>();		
		projectList.setSelectionModel(selectionModel);
		
		verticalPanelProject.add(projectList);
		
		/* ------- Panel sprint ------- */
		verticalPanelSprint = new VerticalPanel();
		Label labelSprint = new Label("Sprint");
		labelSprint.setStyleName("labels");
		Button addSprintButton = new Button("Add Sprint");
		deleteSprintButton = new Button("Delete Sprint");		
		deleteSprintButton.setEnabled(false);
		HorizontalPanel buttonsSprint = new HorizontalPanel();
		buttonsSprint.add(addSprintButton);
		buttonsSprint.add(deleteSprintButton);
		verticalPanelSprint.add(labelSprint);
		verticalPanelSprint.add(buttonsSprint);
		//verticalPanelSprint.setStyleName("verticalPanels");
		verticalPanelSprint.setSize("250px", "100%");
		verticalPanelSprint.getElement().setAttribute("cellspacing", "10");
		addSprintButton.setStyleName("buttonsAdd");
		deleteSprintButton.setStyleName("buttonsDel");
		
		ProvidesKey<ISprint> sprintKeyProvider = new ProvidesKey<ISprint>() {
			public Object getKey(ISprint item) {
				return (item == null) ? null : item.getId();
			}
		};
		// Create a CellList using the keyProvider, for the sprints list
		sprintList = new CellList<ISprint>(new SprintCell(), sprintKeyProvider);
		final SingleSelectionModel<ISprint> selectionModelSprint = new SingleSelectionModel<ISprint>();		
		sprintList.setSelectionModel(selectionModelSprint);
		sprintList.setVisible(false);
		verticalPanelSprint.add(sprintList);
		
		addSprintButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				AddSprintDialog dialog = new AddSprintDialog(agileDashbord.this, selectedProject);
				dialog.center();
			}
		});
		
		deleteSprintButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				IProject selectedProject = selectionModel.getSelectedObject();
				ISprint selectedSprint = selectionModelSprint.getSelectedObject();
				if (selectedProject != null && selectedSprint != null) {
					deleteSprint(selectedProject, selectedSprint);
					loadSprintsProject(selectedProject);
				}
			}
		});
			
		/* ------- Panel task ------- */
		verticalPanelTask = new VerticalPanel();
		Label labelTask = new Label("Task");
		labelTask.setStyleName("labels");
		Button addTaskButton = new Button("Add Task");
		deleteTaskButton = new Button("Delete Task");
		deleteTaskButton.setEnabled(false);
		HorizontalPanel buttonsTask = new HorizontalPanel();
		buttonsTask.add(addTaskButton);
		buttonsTask.add(deleteTaskButton);
		buttonsTask.setStyleName("buttons");
		verticalPanelTask.add(labelTask);
		verticalPanelTask.add(buttonsTask);
		//verticalPanelTask.setStyleName("verticalPanels");
		verticalPanelTask.setSize("250px", "100%");
		verticalPanelTask.getElement().setAttribute("cellspacing", "10");
		addTaskButton.setStyleName("buttonsAdd");
		deleteTaskButton.setStyleName("buttonsDel");
		
		ProvidesKey<ITask> taskKeyProvider = new ProvidesKey<ITask>() {
			public Object getKey(ITask item) {
				return (item == null) ? null : item.getId();
			}
		};
		// Create a CellList using the keyProvider, for the tasks list
		taskList = new CellList<ITask>(new TaskCell(), taskKeyProvider);
		final SingleSelectionModel<ITask> selectionModelTask = new SingleSelectionModel<ITask>();		
		taskList.setSelectionModel(selectionModelTask);
		taskList.setVisible(false);
		verticalPanelTask.add(taskList);
		
		addTaskButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				AddTaskDialog dialog = new AddTaskDialog(agileDashbord.this, selectedSprint);
				dialog.center();
			}
		});
		
		deleteTaskButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				ISprint selectedSprint = selectionModelSprint.getSelectedObject();
				ITask selectedTask = selectionModelTask.getSelectedObject();
				if (selectedSprint != null && selectedTask != null) {
					deleteTask(selectedSprint, selectedTask);
					loadTasksSprint(selectedSprint);
				}				
			}
		});
		
		/* ------- Panel developer ------- */
		verticalPanelDeveloper = new VerticalPanel();
		Label labelDeveloper = new Label("Developer");
		labelDeveloper.setStyleName("labels");
		Button addDeveloperButton = new Button("Add Developer");
		deleteDeveloperButton = new Button("Delete Developer");
		deleteDeveloperButton.setEnabled(false);
		HorizontalPanel buttonsDeveloper = new HorizontalPanel();
		buttonsDeveloper.add(addDeveloperButton);
		buttonsDeveloper.add(deleteDeveloperButton);
		buttonsDeveloper.setStyleName("buttons");
		verticalPanelDeveloper.add(labelDeveloper);
		verticalPanelDeveloper.add(buttonsDeveloper);
		//verticalPanelDeveloper.setStyleName("verticalPanels");
		verticalPanelDeveloper.setSize("250px", "100%");
		verticalPanelDeveloper.getElement().setAttribute("cellspacing", "10");
		addDeveloperButton.setStyleName("buttonsAdd");
		deleteDeveloperButton.setStyleName("buttonsDel");
		
		ProvidesKey<IDeveloper> developerKeyProvider = new ProvidesKey<IDeveloper>() {
			public Object getKey(IDeveloper item) {
				return (item == null) ? null : item.getId();
			}
		};
		// Create a CellList using the keyProvider, for the tasks list
		developerList = new CellList<IDeveloper>(new DeveloperCell(), developerKeyProvider);
		final SingleSelectionModel<IDeveloper> selectionModelDeveloper = new SingleSelectionModel<IDeveloper>();		
		developerList.setSelectionModel(selectionModelDeveloper);
		developerList.setVisible(false);
		verticalPanelDeveloper.add(developerList);
		
		addDeveloperButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				AddDeveloperDialog dialog = new AddDeveloperDialog(agileDashbord.this, selectedTask);
				dialog.center();
			}
		});
		
		deleteDeveloperButton.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				IDeveloper selectedDeveloper = selectionModelDeveloper.getSelectedObject();
				ITask selectedTask = selectionModelTask.getSelectedObject();
				if (selectedDeveloper != null && selectedTask != null) {
					deleteDeveloper(selectedDeveloper, selectedTask);
					loadDevelopers(selectedTask);
				}
			}
		});
		
		globalPanel.add(verticalPanelProject);
		globalPanel.add(verticalPanelSprint);
		globalPanel.add(verticalPanelTask);
		globalPanel.add(verticalPanelDeveloper);
		
		rootPanel.add(globalPanel);
		
		/* -------- CLICK -------- */
		// Events when an element is selected in the list of project
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent arg0) {
				selectedProject = selectionModel.getSelectedObject();
				if (selectedProject != null) {
					deleteProjectButton.setEnabled(true);
					sprintList.setVisible(true);					
					loadSprintsProject(selectedProject);
				}
			}
		});
		
		// Events when an element is selected in the list of sprint
		selectionModelSprint.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent arg0) {
				selectedSprint = selectionModelSprint.getSelectedObject();
				if (selectedSprint != null) {
					deleteSprintButton.setEnabled(true);
					taskList.setVisible(true);
					developerList.setVisible(true);
					loadTasksSprint(selectedSprint);
					//loadDeveloperTask(selectedSprint)
					
				}
			}
		});
		
		// Events when an element is selected in the list of task
		selectionModelTask.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent arg0) {
				selectedTask = selectionModelTask.getSelectedObject();
				if (selectedTask != null) {
					deleteTaskButton.setEnabled(true);
					developerList.setVisible(true);
					loadDevelopers(selectedTask);
				}
			}
		});
		
		// Events when an element is selected in the list of developer
		selectionModelDeveloper.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent arg0) {
				selectedDeveloper = selectionModelDeveloper.getSelectedObject();
				if (selectedDeveloper != null) {
					deleteDeveloperButton.setEnabled(true);
				}
			}
		});
		
		loadAllProjects();
	}
	
	private void loadAllProjects() {		
		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, URL.encode(REST_API_URL + "/projects/findall"));
		rb.setCallback(new RequestCallback() {

			@Override
			public void onError(Request arg0, Throwable arg1) {
				Window.alert("Error while retrieving the list of projects");			
			}

			@Override
			public void onResponseReceived(Request arg0, Response arg1) {
				if(arg1.getStatusCode() == 200){
					IProjects p = ProjectsJsonConverter.getInstance().deserializeFromJson(arg1.getText());
					projectList.setRowCount(p.getProjects().size(), true);
					projectList.setRowData(p.getProjects());
				}
			}						
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while retrieving the list of projects");
		}
	}
	
	protected void addProject(String name) {
		RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, URL.encode(REST_API_URL + "/projects/add"));
		rb.setHeader("Content-Type","application/json; charset=utf8");
		IProject project = ProjectJsonConverter.getInstance().makeProject();
		project.setNom(name);
		String projectJson = ProjectJsonConverter.getInstance().serializeToJson(project);
		rb.setRequestData(projectJson);
		rb.setCallback(new RequestCallback() {			
			public void onResponseReceived(Request req, Response res) {
				if (res.getStatusCode() == 200) {
					loadAllProjects();
				}				
			}			
			public void onError(Request request, Throwable exception) {
				Window.alert("Error while adding the project ");			
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while adding the project");	
		}
	}
	
	private void deleteProject(IProject project) {		
		RequestBuilder rb = new RequestBuilder(RequestBuilder.DELETE, URL.encode(REST_API_URL + "/projects/delete/" +project.getId()));
		rb.setCallback(new RequestCallback() {
			public void onError(Request req, Throwable th) {
				Window.alert("Error while deleting project");
			}
			public void onResponseReceived(Request req, Response res) {				
				selectedProject = null;
				deleteProjectButton.setEnabled(false);
				loadAllProjects();
			}			
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while deleting project");
		}
	}
	
	protected void loadSprintsProject(IProject project) {
		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, URL.encode(REST_API_URL + "/sprints/findSprint/" + project.getId()));
		rb.setCallback(new RequestCallback() {			
			@Override
			public void onResponseReceived(Request arg0, Response arg1) {
				if(arg1.getStatusCode() == 200){
					ISprints sprints = SprintsJsonConverter.getInstance().deserializeFromJson(arg1.getText());
					sprintList.setRowCount(sprints.getSprints().size(), true);
					sprintList.setRowData(sprints.getSprints());
				}
			}
			
			@Override
			public void onError(Request arg0, Throwable arg1) {
				Window.alert("Error while retrieving the list of sprints");
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while retrieving the list of sprints");
		}
	}
	
	protected void addSprint(final IProject project, String name, String target) {
		RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, URL.encode(REST_API_URL + "/sprints/add/"+project.getId()));
		rb.setHeader("Content-Type","application/json; charset=utf8");
		ISprint sprint = SprintJsonConverter.getInstance().makeSprint();
		sprint.setNom(name);
		sprint.setTarget(target);
		String sprintJson = SprintJsonConverter.getInstance().serializeToJson(sprint);
		rb.setRequestData(sprintJson);
		rb.setCallback(new RequestCallback() {			
			public void onResponseReceived(Request req, Response res) {
				if (res.getStatusCode() == 201) {
					loadSprintsProject(project);
				}				
			}			
			public void onError(Request request, Throwable exception) {
				Window.alert("Error while adding the sprint ");			
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while adding the sprint");	
		}
	}

	protected void deleteSprint(final IProject project, ISprint sprint) {
		RequestBuilder rb = new RequestBuilder(RequestBuilder.DELETE, URL.encode(REST_API_URL + "/sprints/delete/" +sprint.getId()));
		rb.setCallback(new RequestCallback() {
			public void onError(Request req, Throwable th) {
				Window.alert("Error while deleting sprint");
			}
			public void onResponseReceived(Request req, Response res) {				
				selectedSprint = null;
				deleteSprintButton.setEnabled(false);
				loadSprintsProject(project);
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while deleting project");
		}
	}

	protected void loadTasksSprint(ISprint sprint) {
		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, URL.encode(REST_API_URL + "/tasks/findTask/" + sprint.getId()));
		rb.setCallback(new RequestCallback() {			
			@Override
			public void onResponseReceived(Request arg0, Response arg1) {
				if(arg1.getStatusCode() == 200){
					ITasks tasks = TasksJsonConverter.getInstance().deserializeFromJson(arg1.getText());
					taskList.setRowCount(tasks.getTasks().size(), true);
					taskList.setRowData(tasks.getTasks());
				}
			}
			
			@Override
			public void onError(Request arg0, Throwable arg1) {
				Window.alert("Error while retrieving the list of tasks");
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while retrieving the list of tasks");
		}
	}
	
	public void addTask(final ISprint sprint, String name, String desc, int prio, String etat) {
		RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, URL.encode(REST_API_URL + "/tasks/add/"+sprint.getId()));
		rb.setHeader("Content-Type","application/json; charset=utf8");
		ITask task = TaskJsonConverter.getInstance().makeTask();
		task.setNom(name);
		task.setDescription(desc);
		task.setPriorite(prio);
		task.setEtat(etat);
		String taskJson = TaskJsonConverter.getInstance().serializeToJson(task);
		rb.setRequestData(taskJson);
		rb.setCallback(new RequestCallback() {			
			public void onResponseReceived(Request req, Response res) {
				if (res.getStatusCode() == 201) {
					loadTasksSprint(sprint);
				}				
			}			
			public void onError(Request request, Throwable exception) {
				Window.alert("Error while adding the tasks ");			
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while adding the project");	
		}	
	}
	
	protected void deleteTask(final ISprint sprint, ITask task) {
		RequestBuilder rb = new RequestBuilder(RequestBuilder.DELETE, URL.encode(REST_API_URL + "/tasks/delete/" +task.getId()));
		rb.setCallback(new RequestCallback() {
			public void onError(Request req, Throwable th) {
				Window.alert("Error while deleting task");
			}
			public void onResponseReceived(Request req, Response res) {				
				selectedTask = null;
				deleteTaskButton.setEnabled(false);
				loadTasksSprint(sprint);
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while deleting task");
		}
	}

	protected void loadDevelopers(ITask task) {
		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, URL.encode(REST_API_URL + "/developers/findDevTask/" + task.getId()));
		rb.setCallback(new RequestCallback() {			
			@Override
			public void onResponseReceived(Request arg0, Response arg1) {
				if(arg1.getStatusCode() == 200){
					IDevelopers developers = DevelopersJsonConverter.getInstance().deserializeFromJson(arg1.getText());
					developerList.setRowCount(developers.getDevelopers().size(), true);
					developerList.setRowData(developers.getDevelopers());
				}
			}
			
			@Override
			public void onError(Request arg0, Throwable arg1) {
				Window.alert("Error while retrieving the list of developers");
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while retrieving the list of developers");
		}
		
	}
	
	protected void addDeveloper(String nom, final ITask task){
		
		RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, URL.encode(REST_API_URL + "/developers/addDev/"+task.getId()));
		rb.setHeader("Content-Type","application/json; charset=utf8");
		final IDeveloper developer = DeveloperJsonConverter.getInstance().makeDeveloper();
		developer.setNom(nom);
		String developerJson = DeveloperJsonConverter.getInstance().serializeToJson(developer);
		rb.setRequestData(developerJson);
		rb.setCallback(new RequestCallback() {			
			public void onResponseReceived(Request req, Response res) {
				if (res.getStatusCode() == 200) {
					loadDevelopers(task);
				}				
			}			
			public void onError(Request request, Throwable exception) {
				Window.alert("Error while adding the developer ");			
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while adding the developer");	
		}
		
	}

	protected void deleteDeveloper(IDeveloper developer, final ITask task) {
		RequestBuilder rb = new RequestBuilder(RequestBuilder.DELETE, URL.encode(REST_API_URL + "/developers/deleteDev/" +developer.getId()+"/"+task.getId()));
		rb.setCallback(new RequestCallback() {
			public void onError(Request req, Throwable th) {
				Window.alert("Error while deleting developer");
			}
			public void onResponseReceived(Request req, Response res) {				
				selectedDeveloper = null;
				deleteDeveloperButton.setEnabled(false);
				loadDevelopers(task);
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			Window.alert("Error while deleting developer");
		}
	}	
}

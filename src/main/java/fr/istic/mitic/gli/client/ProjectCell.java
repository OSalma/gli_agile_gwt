/**
 * 
 */
package fr.istic.mitic.gli.client;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.istic.mitic.gli.shared.IProject;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class ProjectCell extends AbstractCell<IProject>{

	/**
	 * The HTML templates used to render a Cell representing an user
	 */
	interface Templates extends SafeHtmlTemplates {		
		@SafeHtmlTemplates.Template("<div>"
				  + "<span class='project-name'>{0}</span>"
				  + "</div>"
				  + "<div class='project-id'>{1}</div>")
		SafeHtml makeProjectCell( String projectName, int idProject);
	}
	
	/**
	 * Create a singleton instance of the templates used to render the cell.
	 */
	private static Templates templates = GWT.create(Templates.class);
	
	@Override
	public void render(Context arg0, IProject arg1, SafeHtmlBuilder arg2) {
		if (arg1 != null) {
			SafeHtml project_cell_rendered = templates.makeProjectCell(arg1.getNom(), arg1.getId());
			arg2.append(project_cell_rendered);
		}		
	}

}

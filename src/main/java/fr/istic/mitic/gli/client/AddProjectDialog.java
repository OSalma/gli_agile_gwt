/**
 * 
 */
package fr.istic.mitic.gli.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class AddProjectDialog extends DialogBox{
	
	agileDashbord view;

	/**
	 * 
	 */
	public AddProjectDialog(agileDashbord aD) {
		
		this.view = aD;
		
		/* Title of the dialog box */
		setText("Add project");

		setAnimationEnabled(true);
		setGlassEnabled(true);
		setAutoHideEnabled(true);

		/* Name field */
		Label nameLabel = new Label("Name Project");
		final TextBox tName = new TextBox();
		tName.setWidth("120px");
		HorizontalPanel namePanel = new HorizontalPanel();
		namePanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		namePanel.setSpacing(3);
		namePanel.add(nameLabel);
		namePanel.add(tName);


		/* Add all in a vertical panel */
		VerticalPanel panel = new VerticalPanel();
		panel.setHeight("100");
		panel.setWidth("300");
		panel.setSpacing(10);
		panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		panel.add(namePanel);


		/* OK button */
		Button ok = new Button("Valider");
		ok.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				String projectName = tName.getText().trim();			
				if (projectName == "") {
					Window.alert("Please enter a correct name of project");
				} else {
					view.addProject(projectName);
					AddProjectDialog.this.hide();
				}          
			}
		});

		/* Add validate button */
		panel.add(ok);

		setWidget(panel);
	}

}

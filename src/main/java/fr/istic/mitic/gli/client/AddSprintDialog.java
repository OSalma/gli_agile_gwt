/**
 * 
 */
package fr.istic.mitic.gli.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.istic.mitic.gli.shared.IProject;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class AddSprintDialog extends DialogBox{
	
	agileDashbord view;
	IProject project;
	
	/**
	 * 
	 */
	public AddSprintDialog(agileDashbord aD, IProject p) {
		
		this.view = aD;
		this.project = p;
		
		setText("Add sprint");
		
		setAnimationEnabled(true);
		setGlassEnabled(true);
		setAutoHideEnabled(true);
		
		Label nameLabel = new Label("Name Sprint");
		final TextBox tName = new TextBox();
		tName.setWidth("120px");
		HorizontalPanel namePanel = new HorizontalPanel();
		namePanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		namePanel.setSpacing(3);
		namePanel.add(nameLabel);
		namePanel.add(tName);
		
		Label targetLabel = new Label("Target Sprint");
		final TextBox tTarget = new TextBox();
		tTarget.setWidth("120px");
		HorizontalPanel targetPanel = new HorizontalPanel();
		targetPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		targetPanel.setSpacing(3);
		targetPanel.add(targetLabel);
		targetPanel.add(tTarget);
		
		/* Add all in a vertical panel */
		VerticalPanel panel = new VerticalPanel();
		panel.setHeight("100");
		panel.setWidth("300");
		panel.setSpacing(10);
		panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		panel.add(namePanel);
		panel.add(targetPanel);
		
		/* OK button */
		Button ok = new Button("Valider");		
		ok.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent arg0) {
				String sprintName = tName.getText().trim();
				String sprintTarget = tTarget.getText().trim();				
				if (sprintName == "") {
					Window.alert("Entrez un nom correct");
				} else if (sprintTarget == "") {
					Window.alert("Entrez une target correcte");
				} else {
					view.addSprint(project, sprintName, sprintTarget);
					AddSprintDialog.this.hide();
				}  
			}
		});
		
		panel.add(ok);

		setWidget(panel);
	}

}

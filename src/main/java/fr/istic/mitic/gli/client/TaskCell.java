/**
 * 
 */
package fr.istic.mitic.gli.client;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.istic.mitic.gli.shared.ITask;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class TaskCell extends AbstractCell<ITask> {
	
	/**
	 * The HTML templates used to render a Cell representing an user
	 */
	interface Templates extends SafeHtmlTemplates {		
		@SafeHtmlTemplates.Template("<div>"
				  + "<span class='task-name'>{0}</span>"
				  + "</div>"
				  + "<div class='task-description'>{1}</div>"
				  + "<div class='task-priorite'>{2}</div>"
				  + "<div class='task-etat'>{3}</div>")
		SafeHtml makeTaskCell(String projectName,String description, int priorite, String etat);
	}
	
	/**
	 * Create a singleton instance of the templates used to render the cell.
	 */
	private static Templates templates = GWT.create(Templates.class);

	@Override
	public void render(Context arg0, ITask arg1, SafeHtmlBuilder arg2) {
		
		if (arg1 != null) {
			SafeHtml project_cell_rendered = templates.makeTaskCell(arg1.getNom(),
					arg1.getDescription(), arg1.getPriorite(), arg1.getEtat());
			arg2.append(project_cell_rendered);
		}
	}	
}

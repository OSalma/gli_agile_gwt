/**
 * 
 */
package fr.istic.mitic.gli.client;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.istic.mitic.gli.shared.ISprint;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class SprintCell extends AbstractCell<ISprint>{
	
	/**
	 * The HTML templates used to render a Cell representing an user
	 */
	interface Templates extends SafeHtmlTemplates {		
		@SafeHtmlTemplates.Template("<div>"
				  + "<span class='sprint-name'>{0}</span>"
				  + "</div>"
				  + "<div class='sprint-target'>{1}</div>")
		SafeHtml makeSprintCell(String sprintName, String sprintTarget);
	}
	
	/**
	 * Create a singleton instance of the templates used to render the cell.
	 */
	private static Templates templates = GWT.create(Templates.class);
	
	@Override
	public void render(Context arg0, ISprint arg1, SafeHtmlBuilder arg2) {
		if (arg1 != null) {
			SafeHtml sprint_cell_rendered = templates.makeSprintCell(arg1.getNom(), arg1.getTarget());
			arg2.append(sprint_cell_rendered);
		}
	}

}

/**
 * 
 */
package fr.istic.mitic.gli.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.istic.mitic.gli.shared.ISprint;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class AddTaskDialog extends DialogBox{

	agileDashbord view;
	ISprint sp;
	
	/**
	 * 
	 */
	public AddTaskDialog(agileDashbord aD, final ISprint sprint) {
		
		this.view = aD;
		this.sp = sprint;
		
		setText("Add Task");
		
		setAnimationEnabled(true);
		setGlassEnabled(true);
		setAutoHideEnabled(true);
		
		Label nameLabel = new Label("Name Task");
		final TextBox tName = new TextBox();
		tName.setWidth("120px");
		HorizontalPanel namePanel = new HorizontalPanel();
		namePanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		namePanel.setSpacing(3);
		namePanel.add(nameLabel);
		namePanel.add(tName);
		
		Label descLabel = new Label("Description Task");
		final TextArea tDesc = new TextArea();
		tDesc.setWidth("150px");
		tDesc.setHeight("45px");
		HorizontalPanel descPanel = new HorizontalPanel();
		descPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		descPanel.setSpacing(3);
		descPanel.add(descLabel);
		descPanel.add(tDesc);
		
		Label prioLabel = new Label("Priorite Task(int)");
		final IntegerBox tPrio = new IntegerBox();
		tPrio.setWidth("120px");
		HorizontalPanel prioPanel = new HorizontalPanel();
		prioPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		prioPanel.setSpacing(3);
		prioPanel.add(prioLabel);
		prioPanel.add(tPrio);
		
		Label etatLabel = new Label("Etat Task");
		final TextBox tEtat = new TextBox();
		tEtat.setWidth("120px");
		HorizontalPanel etatPanel = new HorizontalPanel();
		etatPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		etatPanel.setSpacing(3);
		etatPanel.add(etatLabel);
		etatPanel.add(tEtat);
		
		VerticalPanel panel = new VerticalPanel();
		panel.setHeight("100");
		panel.setWidth("300");
		panel.setSpacing(10);
		panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		panel.add(namePanel);
		panel.add(descPanel);
		panel.add(prioPanel);
		panel.add(etatPanel);
		
		Button ok = new Button("Valider");
		ok.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent arg0) {
				String taskName = tName.getText().trim();
				String taskDesc = tDesc.getText().trim();
				int taskPrio = tPrio.getValue();
				String taskEtat = tEtat.getText().trim();
				if (taskName == "") {
					Window.alert("Please enter a correct name");
				} else if (taskDesc == "") {
					Window.alert("Please enter a correct description");
				} else if (taskPrio < 0) {
					Window.alert("Please enter priorite en int");
				} else if (taskEtat == ""){
					Window.alert("Please enter a correct etat");
				}else {
					view.addTask(sprint, taskName, taskDesc, taskPrio, taskEtat);
					AddTaskDialog.this.hide();
				}
			
			}
		});
		
		panel.add(ok);
		
		setWidget(panel);
	}

}

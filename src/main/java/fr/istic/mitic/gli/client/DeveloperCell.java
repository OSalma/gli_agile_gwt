/**
 * 
 */
package fr.istic.mitic.gli.client;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.istic.mitic.gli.shared.IDeveloper;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class DeveloperCell extends AbstractCell<IDeveloper> {

	/**
	 * 
	 */
	public DeveloperCell() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * The HTML templates used to render a Cell representing an user
	 */
	interface Templates extends SafeHtmlTemplates {		
		@SafeHtmlTemplates.Template("<div class='developers-list'>"
								  	+ "<div class='developer-id'>Developer {0}</div>"
								  	+ "<div class='developer-nom'> {1}</div>"
								  	+ "</div>")
		SafeHtml makeDeveloperCell(int idDeveloper, String DeveloperNom);
	}
	
	/**
	 * Create singleton instances of the templates used to render the cell.
	 */
	private static Templates templates = GWT.create(Templates.class);

	@Override
	public void render(Context arg0, IDeveloper arg1, SafeHtmlBuilder arg2) {
		if (arg1 != null) {
			// Use the template to create the HTML of an user Cell
			SafeHtml user_cell_rendered = templates.makeDeveloperCell(arg1.getId(), arg1.getNom());
			arg2.append(user_cell_rendered);
		}
	}

}

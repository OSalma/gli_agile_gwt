/**
 * 
 */
package fr.istic.mitic.gli.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.mitic.gli.shared.ITask;
import fr.istic.mitic.gli.shared.MyFactory;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class TaskJsonConverter {
	
	private TaskJsonConverter() {}
	
	private static TaskJsonConverter instance = new TaskJsonConverter();
	
	// Instantiate the factory
	MyFactory factory = GWT.create(MyFactory.class);
	
	public ITask makeTask() {
		AutoBean<ITask> task = factory.task();
		return task.as();
	}
	
	public String serializeToJson(ITask task) {
		AutoBean<ITask> bean = AutoBeanUtils.getAutoBean(task);

		return AutoBeanCodex.encode(bean).getPayload();
	}
	
	public ITask deserializeFromJson(String json) {
		AutoBean<ITask> bean = AutoBeanCodex.decode(factory, ITask.class, json);
		return bean.as();
	}

	/**
	 * 
	 */
	public static TaskJsonConverter getInstance() {
		return instance;
	}

}

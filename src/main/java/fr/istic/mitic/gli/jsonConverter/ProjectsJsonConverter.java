/**
 * 
 */
package fr.istic.mitic.gli.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.mitic.gli.shared.IProjects;
import fr.istic.mitic.gli.shared.MyFactory;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class ProjectsJsonConverter {

	private ProjectsJsonConverter() {}
	
	private static ProjectsJsonConverter instance = new ProjectsJsonConverter();
	
	// Instantiate the factory
	MyFactory factory = GWT.create(MyFactory.class);
	
	public IProjects makeProjects() {
		AutoBean<IProjects> projects = factory.projects();
		return projects.as();
	}
	
	public String serializeToJson(IProjects projects) {
		AutoBean<IProjects> bean = AutoBeanUtils.getAutoBean(projects);

		return AutoBeanCodex.encode(bean).getPayload();
	}
	
	public IProjects deserializeFromJson(String json) {
		AutoBean<IProjects> bean = AutoBeanCodex.decode(factory, IProjects.class,  "{\"projects\":" + json + "}");
		return bean.as();
	}

	public static ProjectsJsonConverter getInstance() {
		return instance;
	}
}
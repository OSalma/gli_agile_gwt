/**
 * 
 */
package fr.istic.mitic.gli.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.mitic.gli.shared.IDevelopers;
import fr.istic.mitic.gli.shared.MyFactory;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class DevelopersJsonConverter {

	/**
	 * 
	 */
	public DevelopersJsonConverter() {
		// TODO Auto-generated constructor stub
	}
	
	private static DevelopersJsonConverter instance = new DevelopersJsonConverter();
	
	MyFactory factory = GWT.create(MyFactory.class);
	
	public IDevelopers makeDevelopers() {
		AutoBean<IDevelopers> developers = factory.develoeprs();
		return developers.as();
	}
	
	public String serializeToJson(IDevelopers developers) {
		AutoBean<IDevelopers> bean = AutoBeanUtils.getAutoBean(developers);

		return AutoBeanCodex.encode(bean).getPayload();
	}
	
	public IDevelopers deserializeFromJson(String json) {
		AutoBean<IDevelopers> bean = AutoBeanCodex.decode(factory, IDevelopers.class,  "{\"developers\":" + json + "}");
		return bean.as();
	}

	public static DevelopersJsonConverter getInstance() {
		return instance;
	}

}

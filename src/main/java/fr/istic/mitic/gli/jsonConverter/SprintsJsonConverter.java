/**
 * 
 */
package fr.istic.mitic.gli.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.mitic.gli.shared.ISprints;
import fr.istic.mitic.gli.shared.MyFactory;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class SprintsJsonConverter {

	/**
	 * 
	 */
	public SprintsJsonConverter() {
		// TODO Auto-generated constructor stub
	}
	
	private static SprintsJsonConverter instance = new SprintsJsonConverter();
	
	MyFactory factory = GWT.create(MyFactory.class);
	
	public ISprints makeSprints() {
		AutoBean<ISprints> sprints = factory.sprints();
		return sprints.as();
	}
	
	public String serializeToJson(ISprints sprints) {
		AutoBean<ISprints> bean = AutoBeanUtils.getAutoBean(sprints);
		return AutoBeanCodex.encode(bean).getPayload();
	}
	
	public ISprints deserializeFromJson(String json) {
		AutoBean<ISprints> bean = AutoBeanCodex.decode(factory, ISprints.class,  "{\"sprints\":" + json + "}");
		return bean.as();
	}
	
	public static SprintsJsonConverter getInstance() {
		return instance;
	}

}

/**
 * 
 */
package fr.istic.mitic.gli.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.mitic.gli.shared.ITasks;
import fr.istic.mitic.gli.shared.MyFactory;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class TasksJsonConverter {

	/**
	 * 
	 */
	public TasksJsonConverter() {
		// TODO Auto-generated constructor stub
	}
	
	private static TasksJsonConverter instance = new TasksJsonConverter();
	
	
	MyFactory factory = GWT.create(MyFactory.class);
	
	public ITasks makeTasks() {
		AutoBean<ITasks> tasks = factory.tasks();
		return tasks.as();
	}
	
	public String serializeToJson(ITasks tasks) {
		AutoBean<ITasks> bean = AutoBeanUtils.getAutoBean(tasks);
		return AutoBeanCodex.encode(bean).getPayload();
	}
	
	public ITasks deserializeFromJson(String json) {
		AutoBean<ITasks> bean = AutoBeanCodex.decode(factory, ITasks.class,  "{\"tasks\":" + json + "}");
		return bean.as();
	}
	
	public static TasksJsonConverter getInstance() {
		return instance;
	}

}

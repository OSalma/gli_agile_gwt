/**
 * 
 */
package fr.istic.mitic.gli.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.mitic.gli.shared.IProject;
import fr.istic.mitic.gli.shared.MyFactory;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class ProjectJsonConverter {

	private ProjectJsonConverter() {}
	
	private static ProjectJsonConverter instance = new ProjectJsonConverter();
	
	// Instantiate the factory
	MyFactory factory = GWT.create(MyFactory.class);
	
	public IProject makeProject() {
		AutoBean<IProject> project = factory.project();
		return project.as();
	}
	
	public String serializeToJson(IProject project) {
		AutoBean<IProject> bean = AutoBeanUtils.getAutoBean(project);

		return AutoBeanCodex.encode(bean).getPayload();
	}
	
	public IProject deserializeFromJson(String json) {
		AutoBean<IProject> bean = AutoBeanCodex.decode(factory, IProject.class, json);
		return bean.as();
	}

	public static ProjectJsonConverter getInstance() {
		return instance;
	}

}

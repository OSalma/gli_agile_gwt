/**
 * 
 */
package fr.istic.mitic.gli.jsonConverter;

import com.google.gwt.core.shared.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.mitic.gli.shared.IDeveloper;
import fr.istic.mitic.gli.shared.MyFactory;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class DeveloperJsonConverter {

	/**
	 * 
	 */
	private DeveloperJsonConverter() {
		// TODO Auto-generated constructor stub
	}
	
	private static DeveloperJsonConverter instance = new DeveloperJsonConverter();
	
	MyFactory factory = GWT.create(MyFactory.class);
	
	public IDeveloper makeDeveloper() {
		AutoBean<IDeveloper> developer = factory.developer();
		return developer.as();
	}
	
	public String serializeToJson(IDeveloper developer) {
		// Retrieve the AutoBean controller
		AutoBean<IDeveloper> bean = AutoBeanUtils.getAutoBean(developer);

		return AutoBeanCodex.encode(bean).getPayload();
	}
	
	public IDeveloper deserializeFromJson(String json) {
		AutoBean<IDeveloper> bean = AutoBeanCodex.decode(factory, IDeveloper.class, json);
		return bean.as();
	}
	
	public static DeveloperJsonConverter getInstance() {
		return instance;
	}

}

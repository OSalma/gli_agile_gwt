/**
 * 
 */
package fr.istic.mitic.gli.jsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

import fr.istic.mitic.gli.shared.ISprint;
import fr.istic.mitic.gli.shared.MyFactory;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public class SprintJsonConverter {
	
	private SprintJsonConverter() {}
	
	private static SprintJsonConverter instance = new SprintJsonConverter();

	// Instantiate the factory
	MyFactory factory = GWT.create(MyFactory.class);
	
	public ISprint makeSprint() {
		AutoBean<ISprint> sprint = factory.sprint();
		return sprint.as();
	}
	
	public String serializeToJson(ISprint sprint) {
		AutoBean<ISprint> bean = AutoBeanUtils.getAutoBean(sprint);

		return AutoBeanCodex.encode(bean).getPayload();
	}
	
	public ISprint deserializeFromJson(String json) {
		AutoBean<ISprint> bean = AutoBeanCodex.decode(factory, ISprint.class, json);
		return bean.as();
	}
	
	public static SprintJsonConverter getInstance() {
		return instance;
	}

}

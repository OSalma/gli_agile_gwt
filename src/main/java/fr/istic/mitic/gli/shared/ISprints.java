/**
 * 
 */
package fr.istic.mitic.gli.shared;

import java.util.List;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public interface ISprints {
	
	void setSprints(List<ISprint> sprints);
	
	List<ISprint> getSprints();

}

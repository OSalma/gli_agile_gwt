package fr.istic.mitic.gli.shared;

/**
 * @author Ouhammouch Salma & Assal Mohamed RIda M2 MITIC
 *
 */
public interface IProject {
	
	public int getId();

	public void setId(int id);

	public String getNom();

	public void setNom(String nom);

}

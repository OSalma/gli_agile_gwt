/**
 * 
 */
package fr.istic.mitic.gli.shared;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public interface IDeveloper {
	
	public int getId();

	public void setId(int id);

	public String getNom();

	public void setNom(String nom);

}

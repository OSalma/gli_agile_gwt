/**
 * 
 */
package fr.istic.mitic.gli.shared;

import java.util.List;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public interface IDevelopers {
	
	void setDevelopers(List<IDeveloper> developers);
	
	List<IDeveloper> getDevelopers();

}

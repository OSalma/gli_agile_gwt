/**
 * 
 */
package fr.istic.mitic.gli.shared;


/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public interface ITask {
	
	public Long getId();

	public void setId(Long id);
	
	public String getNom();

	public void setNom(String nom);

	public String getDescription();

	public void setDescription(String description);

	public int getPriorite();

	public void setPriorite(int priorite);

	public String getEtat();

	public void setEtat(String etat);

}

/**
 * 
 */
package fr.istic.mitic.gli.shared;

import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public interface MyFactory extends AutoBeanFactory{
	
	AutoBean<IProject> project();
	AutoBean<IProjects> projects();
	
	AutoBean<ISprint> sprint();
	AutoBean<ISprints> sprints();
	
	AutoBean<ITask> task();
	AutoBean<ITasks> tasks();
	
	AutoBean<IDeveloper> developer();
	AutoBean<IDevelopers> develoeprs();

}

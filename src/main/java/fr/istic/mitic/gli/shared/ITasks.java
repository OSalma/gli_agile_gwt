/**
 * 
 */
package fr.istic.mitic.gli.shared;

import java.util.List;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public interface ITasks {
	
	void setTasks(List<ITask> tasks);
	
	List<ITask> getTasks();

}

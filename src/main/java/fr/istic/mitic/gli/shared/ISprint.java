/**
 * 
 */
package fr.istic.mitic.gli.shared;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public interface ISprint {
	
	public int getId();

	public void setId(int id);
	
	public String getNom();

	public void setNom(String nom);
	
	public String getTarget();

	public void setTarget(String target);
	
	public int getIdProject();
	
	public void setIdProject(int id);

}

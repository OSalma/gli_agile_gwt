/**
 * 
 */
package fr.istic.mitic.gli.shared;

import java.util.List;

/**
 * @author Ouhammouch Salma & Assal Mohamed Rida M2 MITIC
 *
 */
public interface IProjects {
	
	void setProjects(List<IProject> projects);
	
	List<IProject> getProjects();

}

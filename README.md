# TP GWT Agile  (client)

## Etudiants

Cette application est crée par Ouhammouch Salma & Assal Mohamed Rida 
Etudiants en Master2 MITIC

## Repertoire

Ce repertoire contient l'application client en GWT pour le module GLI

## Lancement

Pour lancer l'application cleint il faut lancer l'application spring d'abord puis lancer l'application client

``` ./run-hsqldb-server.sh ```

``` mvn spring-boot:run ```

``` mvn clean compile gwt:compile package tomcat7:run-war-only ```

Accéder à l'application : (http://localhost:8082/agileDashbord.html)

L'application est lancée sur le port 8082. Ceci est modifiable dans la configuration de tomcat7, dans le fichier [pom.xml]

## Remarque 

Pour tester l'application GWT  avec le serveur d'application de docker vous devez  exécuter juste cette commande " mvn clean compile gwt:compile package tomcat7:run-war-only "